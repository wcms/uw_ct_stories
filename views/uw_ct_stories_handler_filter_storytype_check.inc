<?php

class uw_ct_stories_handler_filter_storytype_check extends views_handler_filter_boolean_operator {

  /**
   * Alters Views query when filter is used.
  */
  function query() {

    $this->ensure_my_table();

    // Get the value from the exposed form
    $value = $this->view->exposed_data['inherit_story_type'];
    $story_type_tid = uw_ct_stories_get_current_story_type_tid();

    // If checked, filter by the Story Type term.
    if (($value == 1) && isset($story_type_tid)) {
      $this->query->add_where($this->options['group'], 'field_data_field_story_type.field_story_type_tid', intval($story_type_tid));
    }
  }
}