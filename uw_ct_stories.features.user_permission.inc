<?php

/**
 * @file
 * uw_ct_stories.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_stories_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create stories_featured content'.
  $permissions['create stories_featured content'] = array(
    'name' => 'create stories_featured content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_stories content'.
  $permissions['create uw_stories content'] = array(
    'name' => 'create uw_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any stories_featured content'.
  $permissions['delete any stories_featured content'] = array(
    'name' => 'delete any stories_featured content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_stories content'.
  $permissions['delete any uw_stories content'] = array(
    'name' => 'delete any uw_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own stories_featured content'.
  $permissions['delete own stories_featured content'] = array(
    'name' => 'delete own stories_featured content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_stories content'.
  $permissions['delete own uw_stories content'] = array(
    'name' => 'delete own uw_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in story_type'.
  $permissions['delete terms in story_type'] = array(
    'name' => 'delete terms in story_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any stories_featured content'.
  $permissions['edit any stories_featured content'] = array(
    'name' => 'edit any stories_featured content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_stories content'.
  $permissions['edit any uw_stories content'] = array(
    'name' => 'edit any uw_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own stories_featured content'.
  $permissions['edit own stories_featured content'] = array(
    'name' => 'edit own stories_featured content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_stories content'.
  $permissions['edit own uw_stories content'] = array(
    'name' => 'edit own uw_stories content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in story_type'.
  $permissions['edit terms in story_type'] = array(
    'name' => 'edit terms in story_type',
    'roles' => array(
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter stories_featured revision log entry'.
  $permissions['enter stories_featured revision log entry'] = array(
    'name' => 'enter stories_featured revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter uw_stories revision log entry'.
  $permissions['enter uw_stories revision log entry'] = array(
    'name' => 'enter uw_stories revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured authored by option'.
  $permissions['override stories_featured authored by option'] = array(
    'name' => 'override stories_featured authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured authored on option'.
  $permissions['override stories_featured authored on option'] = array(
    'name' => 'override stories_featured authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured promote to front page option'.
  $permissions['override stories_featured promote to front page option'] = array(
    'name' => 'override stories_featured promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured published option'.
  $permissions['override stories_featured published option'] = array(
    'name' => 'override stories_featured published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured revision option'.
  $permissions['override stories_featured revision option'] = array(
    'name' => 'override stories_featured revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override stories_featured sticky option'.
  $permissions['override stories_featured sticky option'] = array(
    'name' => 'override stories_featured sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories authored by option'.
  $permissions['override uw_stories authored by option'] = array(
    'name' => 'override uw_stories authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories authored on option'.
  $permissions['override uw_stories authored on option'] = array(
    'name' => 'override uw_stories authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories promote to front page option'.
  $permissions['override uw_stories promote to front page option'] = array(
    'name' => 'override uw_stories promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories published option'.
  $permissions['override uw_stories published option'] = array(
    'name' => 'override uw_stories published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories revision option'.
  $permissions['override uw_stories revision option'] = array(
    'name' => 'override uw_stories revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories sticky option'.
  $permissions['override uw_stories sticky option'] = array(
    'name' => 'override uw_stories sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search stories_featured content'.
  $permissions['search stories_featured content'] = array(
    'name' => 'search stories_featured content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search uw_stories content'.
  $permissions['search uw_stories content'] = array(
    'name' => 'search uw_stories content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
