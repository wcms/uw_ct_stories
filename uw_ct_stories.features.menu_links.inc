<?php

/**
 * @file
 * uw_ct_stories.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_stories_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_story-type:admin/structure/taxonomy/story_type/.
  $menu_links['menu-site-manager-vocabularies_story-type:admin/structure/taxonomy/story_type/'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/story_type/',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Story Type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_story-type:admin/structure/taxonomy/story_type/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_topics:admin/structure/taxonomy/uw_stories_societal_relevance.
  $menu_links['menu-site-manager-vocabularies_topics:admin/structure/taxonomy/uw_stories_societal_relevance'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_stories_societal_relevance',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Topics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_topics:admin/structure/taxonomy/uw_stories_societal_relevance',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Story Type');
  t('Topics');

  return $menu_links;
}
