<?php

/**
 * @file
 * uw_ct_stories.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_stories_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'story_type_landing_pages';
  $context->description = 'List of all stories to be displayed on the Story Type landing pages';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'stories_featured' => 'stories_featured',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_stories-block_1' => array(
          'module' => 'views',
          'delta' => 'uw_stories-block_1',
          'region' => 'content',
          'weight' => '20',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('List of all stories to be displayed on the Story Type landing pages');
  $export['story_type_landing_pages'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_stories_5_top';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'story_type' => 'story_type',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-stories_featured_block-block' => array(
          'module' => 'views',
          'delta' => 'stories_featured_block-block',
          'region' => 'banner',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['uw_stories_5_top'] = $context;

  return $export;
}
